<?php

namespace SmartApp\Yii\Components;

use SmartApp\Domain\SchemaJsonSerializableInterface;
use Spatie\SchemaOrg\Schema;

/**
 * Class SchemaOrgListBuilder
 *
 * @package SmartApp\Yii\Components
 */
class SchemaOrgListBuilder
{
    /**
     * @var SchemaJsonSerializableInterface[]
     */
    private $items;
    
    /**
     * @param SchemaJsonSerializableInterface[] $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }
    
    /**
     * @return string
     */
    public function getItemList()
    {
        $builder = Schema::itemList();
        $items = [];
        foreach ($this->items as $item) {
            $items[] = $item->prepareObject();
        }
        $builder->numberOfItems(count($items));
        $builder->itemListElement($items);
        
        return '<script type="application/ld+json">'
            . json_encode($builder, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)
            . '</script>';
    }
}
