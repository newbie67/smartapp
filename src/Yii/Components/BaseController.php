<?php

namespace SmartApp\Yii\Components;

use SmartApp\Domain\RestaurantStorageInterface;
use SmartApp\Yii\Models\Restaurant\Restaurant;
use yii\di\Container;
use yii\web\Controller;
use Yii;

class BaseController extends Controller
{
    /**
     * @var Container
     */
    protected $container;
    
    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->registerProjectServices();

        parent::init();
    }
    
    /**
     * @return void
     */
    private function registerProjectServices()
    {
        $this->container = Yii::$container;

        $this->container->set(RestaurantStorageInterface::class, function () {
            return Restaurant::instance();
        });
    }
}
