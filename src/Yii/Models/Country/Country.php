<?php

namespace SmartApp\Yii\Models\Country;

use SmartApp\Domain\CountryInterface;
use yii\db\ActiveRecord;

/**
 * @property int    $id
 * @property string $name
 * @property string $code
 */
class Country extends ActiveRecord implements CountryInterface
{
    /**
     * @inheritDoc
     */
    public function getId()
    {
        return (int)$this->id;
    }
    
    /**
     * @inheritDoc
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->name;
    }
}
