<?php

namespace SmartApp\Yii\Models\Restaurant;

use SmartApp\Domain\CountryInterface;
use SmartApp\Domain\OwnerInterface;
use SmartApp\Domain\RestaurantInterface;
use SmartApp\Domain\RestaurantStorageInterface;
use Spatie\SchemaOrg\Schema;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property int            $id
 * @property string|null    $url
 * @property string         $name
 * @property string         $contact_email
 * @property string         $city
 * @property string         $address
 *
 * @property CountryInterface   $country
 * @property OwnerInterface     $owner
 */
class Restaurant extends ActiveRecord implements RestaurantInterface, RestaurantStorageInterface
{
    /**
     * @inheritDoc
     */
    public function getId()
    {
        return (int)$this->id;
    }
    
    /**
     * @inheritDoc
     */
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
     * @inheritDoc
     */
    public function getAbsoluteUrl()
    {
        if (null === $this->getUrl()) {
            return null;
        }
        
        // todo: here must be full working url
        return Yii::$app->request->getHostInfo() . '/restaurant/' .  $this->url;
    }
    
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @inheritDoc
     */
    public function getContactEmail()
    {
        return $this->contact_email;
    }
    
    /**
     * @inheritDoc
     */
    public function getCity()
    {
        return $this->city;
    }
    
    /**
     * @inheritDoc
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * @inheritDoc
     */
    public function getFullAddress()
    {
        return $this->getCity() . ' ' . $this->getAddress();
    }
    
    /**
     * @inheritDoc
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * @inheritDoc
     */
    public function getOwner()
    {
        return $this->owner;
    }
    
    /**
     * @inheritDoc
     */
    public function serializeToLdJson($withScriptTag = true)
    {
        // I use custom json_encode function, caz i like JSON_PRETTY_PRINT
        if ($withScriptTag) {
            return '<script type="application/ld+json">'
                . json_encode($this->prepareObject(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
                . '</script>';
        } else {
            return json_encode($this->prepareObject(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }
    }
    
    /**
     * @inheritDoc
     */
    public function prepareObject()
    {
        $builder = Schema::restaurant();
        
        // Сюда можно ещё кучу полей добавить, я не стал заморачиваться, думаю итак всё понятно
        $builder->name($this->getName())
            ->email($this->getContactEmail())
            ->address($this->getFullAddress())
            ->url($this->getAbsoluteUrl())
        ;
        
        return $builder;
    }
    
    /**
     * @inheritDoc
     */
    public function getById($id)
    {
        return self::find()->where(['id' => $id])->one();
    }
    
    /**
     * @inheritDoc
     */
    public function getAll()
    {
        return self::find()->all();
    }
}
