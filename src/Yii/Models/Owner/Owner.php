<?php

namespace SmartApp\Yii\Models\Country;

use SmartApp\Domain\OwnerInterface;
use yii\db\ActiveRecord;

/**
 * @property int    $id
 * @property string $name
 * @property string $email
 */
class Owner extends ActiveRecord implements OwnerInterface
{
    /**
     * @inheritDoc
     */
    public function getId()
    {
        return (int)$this->id;
    }
    
    /**
     * @inheritDoc
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->name;
    }
}
