<?php

namespace SmartApp\Yii\Controller;

use yii\web\Controller;

class ServiceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}
