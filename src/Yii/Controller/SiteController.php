<?php

namespace SmartApp\Yii\Controller;

use SmartApp\Domain\RestaurantStorageInterface;
use SmartApp\Yii\Components\BaseController;
use SmartApp\Yii\Components\SchemaOrgListBuilder;
use yii\web\HttpException;

class SiteController extends BaseController
{
    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        /** @var RestaurantStorageInterface $restaurantStorage */
        $restaurantStorage = $this->container->get(RestaurantStorageInterface::class);
        $restaurants = $restaurantStorage->getAll();
        $listBuilder = new SchemaOrgListBuilder($restaurants);
        
        return $this->render('index', [
            'restaurants' => $restaurants,
            'listBuilder' => $listBuilder,
        ]);
    }
    
    /**
     * @throws HttpException
     */
    public function actionLogin()
    {
        throw new HttpException('403', 'You are not allowed to access this page');
    }
}
