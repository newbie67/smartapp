<?php

/**
 * @var $this yii\web\View
 * @var \SmartApp\Domain\RestaurantInterface[] $restaurants
 * @var \SmartApp\Yii\Components\SchemaOrgListBuilder $listBuilder
 */

$this->title = 'My Yii Application';
?>

<div class="site-index">
    <h2>ListItem:</h2>
    <pre><?= htmlspecialchars($listBuilder->getItemList()) ?></pre>
    
    <h2>Items by one:</h2>
    <?php foreach ($restaurants as $restaurant) : ?>
        <pre><?= htmlspecialchars($restaurant->serializeToLdJson()) ?></pre>
    <?php endforeach ?>
</div>
