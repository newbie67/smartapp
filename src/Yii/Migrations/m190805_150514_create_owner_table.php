<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%owner}}`.
 */
class m190805_150514_create_owner_table extends Migration
{
    /**
     * @var array
     */
    private $owners = [
        'Алексей Зорин' => 'azegence@gmail.com',
        'Умберто де Факагинез' => 'umbertode@yopmail.com',
    ];
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%owner}}', [
            'id' => $this->primaryKey(),
            'name' => 'VARCHAR(255) NOT NULL',
            'email' => 'VARCHAR(255) NOT NULL',
            'UNIQUE KEY (email)',
        ]);
    
        foreach ($this->owners as $ownerName => $ownerEmail) {
            $this->insert('{{%owner}}', [
                'name' => $ownerName,
                'email' => $ownerEmail,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%owner}}');
    }
}
