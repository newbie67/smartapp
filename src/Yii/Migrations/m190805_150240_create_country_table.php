<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%country}}`.
 */
class m190805_150240_create_country_table extends Migration
{
    /**
     * @var array
     */
    private $countries = [
        'AE' => 'Объединенные Арабские Эмираты',
        'AG' => 'Антигуа и Барбуда',
        'BG' => 'Болгария',
        'AT' => 'Австрия',
        'CA' => 'Канада',
        'CY' => 'Кипр',
        'RU' => 'Россия',
        'CZ' => 'Чехия',
        'NZ' => 'Новая Зеландия',
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%country}}', [
            'id' => $this->primaryKey(),
            'name' => 'VARCHAR(255) NOT NULL',
            'code' => 'VARCHAR(5) NOT NULL',
            'UNIQUE KEY (code)',
        ]);

        foreach ($this->countries as $countryCode => $countryName) {
            $this->insert('{{%country}}', [
                'code' => $countryCode,
                'name' => $countryName,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%country}}');
    }
}
