<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%restaurant}}`.
 */
class m190806_084603_create_restaurant_table extends Migration
{
    /**
     * @var array
     */
    protected $restaurants = [
        [
            'name' => 'Тифлисский    дворик',
            'city' => 'Москва',
            'address' => 'ул. Остоженка, д. 32',
        ],
        [
            'name' => 'Вилла Паста',
            'city' => 'Москва',
            'address' => 'ул. Большая Дмитровка, д. 32',
        ],
        [
            'name' => 'Гави',
            'city' => 'Екатеринбург',
            'address' => 'Большая Дорогомиловская ул., д. 1',
        ],
        [
            'name' => 'Балкан Гриль',
            'city' => 'Краснодар',
            'address' => 'ул. Советская, 35',
        ],
        [
            'name' => 'Угли угли',
            'city' => 'Краснодар',
            'address' => 'ул. Тургенева,  д. 126/1',
        ],
    ];
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%restaurant}}', [
            'id' => $this->primaryKey(),
            'owner_id' => 'INTEGER NOT NULL',
            'country_id' => 'INTEGER NOT NULL',
            'url' => 'VARCHAR(255) DEFAULT NULL',
            'name' => 'VARCHAR(255) NOT NULL',
            'contact_email' => 'VARCHAR(255) NOT NULL',
            'city' => 'VARCHAR(255) NOT NULL',
            'address' => 'VARCHAR(255) NOT NULL',
        ]);
        
        $this->addForeignKey(
            'restaurant_owner',
            '{{%restaurant}}',
            'owner_id',
            '{{%owner}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'restaurant_country',
            '{{%restaurant}}',
            'country_id',
            '{{%country}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        foreach ($this->getRestaurants() as $restarant) {
            $this->insert('{{%restaurant}}', $restarant);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('restaurant_country', '{{%restaurant}}');
        $this->dropForeignKey('restaurant_owner', '{{%restaurant}}');
        $this->dropTable('{{%restaurant}}');
    }
    
    /**
     * @return array
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    private function getRestaurants()
    {
        foreach ($this->restaurants as $k => $restaurant) {
            $url = rand(0,1) === 0 ? null : $this->stringToUrl($restaurant['name']);
            $ownerId = $this->db->createCommand('SELECT id FROM {{%owner}} ORDER BY RAND() LIMIT 1')->queryScalar();
            $countryId = $this->db->createCommand('SELECT id FROM {{%country}} ORDER BY RAND() LIMIT 1')->queryScalar();
            $this->restaurants[$k] = array_merge([
                'url' => $url,
                'owner_id' => $ownerId,
                'country_id' => $countryId,
                'contact_email' => mb_substr(sha1(Yii::$app->getSecurity()->generateRandomString(10)), 0, 10)
                    . '@yopmail.com',
            ], $restaurant);
        }
        return $this->restaurants;
    }
    
    /**
     * @param string $string
     * @return string
     */
    private function stringToUrl($string)
    {
        // Replace custom symbols with '-'
        $string = preg_replace('/[^a-zA-ZА-Яа-яёЁ]/u', '-', $string);
        // Replace 2+ "-" to one "-"
        $string = preg_replace('/[-]+/u', '-', $string);

        return mb_strtolower(trim($string, '-'));
    }
}
