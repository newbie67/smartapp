<?php

namespace SmartApp\Domain;

/**
 * Interface CountryInterface
 *
 * @package SmartApp\Domain
 */
interface CountryInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getCode();
}
