<?php

namespace SmartApp\Domain;

/**
 * Interface OwnerInterface
 *
 * @package SmartApp\Domain
 */
interface OwnerInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getEmail();
}
