<?php

namespace SmartApp\Domain;

/**
 * Interface RestaurantStorageInterface
 *
 * @package SmartApp\Domain
 */
interface RestaurantStorageInterface
{
    /**
     * @param int $id
     *
     * @return RestaurantInterface
     */
    public function getById($id);
    
    /**
     * @return RestaurantInterface[]
     */
    public function getAll();
}
