<?php

namespace SmartApp\Domain;

use Spatie\SchemaOrg\Restaurant;

/**
 * Interface SchemaJsonSerializableInterface
 *
 * @package SmartApp\Domain
 */
interface SchemaJsonSerializableInterface
{
    /**
     * @param bool $withScripTag
     *
     * @return string
     */
    public function serializeToLdJson($withScripTag = true);
    
    /**
     * @return Restaurant
     */
    public function prepareObject();
}
