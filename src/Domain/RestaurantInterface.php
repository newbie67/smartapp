<?php

namespace SmartApp\Domain;

/**
 * Interface RestaurantInterface
 *
 * @package SmartApp\Domain
 */
interface RestaurantInterface extends SchemaJsonSerializableInterface
{
    /**
     * @return int
     */
    public function getId();
    
    /**
     * @return string|null
     */
    public function getUrl();
    
    /**
     * @return string|null
     */
    public function getAbsoluteUrl();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getContactEmail();

    /**
     * @return string
     */
    public function getCity();

    /**
     * @return string
     */
    public function getAddress();

    /**
     * @return string
     */
    public function getFullAddress();

    /**
     * @return CountryInterface
     */
    public function getCountry();

    /**
     * @return OwnerInterface
     */
    public function getOwner();
}
