### Run project
```
# Copy environment variables file and write your config
cp .env.dist .env

# Run docker
docker-compose up -d

# Go to container with PHP
docker-compose exec smartapp_php bash

# install dependencies
composer update

# run migration
php yii migrate/up
```
